from gettext import gettext as _
from gi.repository import Adw
from swatch.base_preferences import (
    MPreferencesPage, MPreferencesGroup, PreferencesToggleRow
)


class AppearancePreferencesPage(MPreferencesPage):
    def __init__(self):
        super().__init__(
            title=_('Appearance'), icon_name='preferences-other-symbolic',
            pref_groups=[
                MPreferencesGroup(
                    title=_('Global appearance'), rows=[
                        PreferencesToggleRow(
                            title=_('Dark mode'),
                            conf_key='dark_mode',
                            signal='dark_mode_changed'
                        )
                    ]
                )
            ]
        )


class PreferencesWindow(Adw.PreferencesWindow):
    def __init__(self):
        super().__init__(default_width=360, default_height=600)
        self.pages = [
            AppearancePreferencesPage(),
        ]
        for p in self.pages:
            self.add(p)
