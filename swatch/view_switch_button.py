from gettext import gettext as _
from gi.repository import Gtk
from swatch.confManager import ConfManager


class ViewSwitchButton(Gtk.Button):
    __gtype_name__ = 'ViewSwitchButton'

    def __init__(self):
        self.confman = ConfManager()
        super().__init__(tooltip_text=_('Change View Mode'))
        self.set_view_list(self.confman.conf['palette_view_list'])

    def get_view_list(self) -> bool:
        return self.confman.conf['palette_view_list']

    def set_view_list(self, view_list: bool):
        self.set_icon_name(
            'view-list-symbolic' if view_list else 'view-grid-symbolic'
        )
        self.confman.conf['palette_view_list'] = view_list
        self.confman.save_conf()
