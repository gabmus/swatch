# from gi.repository import Adw, GLib
from swatch.confManager import ConfManager
from swatch.headerbar import GHeaderbar
from swatch.base_app import BaseWindow, AppShortcut
from swatch.palette_list_view import PaletteListView


class AppWindow(BaseWindow):
    def __init__(self):
        super().__init__(
            app_name='Swatch',
            icon_name='org.gabmus.swatch',
            shortcuts=[AppShortcut(
                'F10', lambda *args: self.headerbar.menu_btn.popup()
            )]
        )
        self.confman = ConfManager()

        self.headerbar = GHeaderbar()
        self.palette_list_view = PaletteListView()
        self.headerbar.add_btn.connect('clicked', self.add_palette)
        self.append(self.headerbar)
        self.append(self.palette_list_view)

        self.confman.connect(
            'dark_mode_changed',
            lambda *args: self.set_dark_mode(self.confman.conf['dark_mode'])
        )
        self.set_dark_mode(self.confman.conf['dark_mode'])
        self.connect('close-request', self.on_destroy)

    def add_palette(self, _):
        self.palette_list_view.add_palette()

    def present(self):
        super().present()
        self.set_default_size(
            self.confman.conf['windowsize']['width'],
            self.confman.conf['windowsize']['height']
        )

    def on_destroy(self, *args):
        self.confman.conf['windowsize'] = {
            'width': self.get_width(),
            'height': self.get_height()
        }
        self.confman.save_conf()
