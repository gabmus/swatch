from gettext import gettext as _
from typing import Union
from gi.repository import Gtk, Adw
from swatch.color import Color
from swatch.confManager import ConfManager
from swatch.palette import Palette
from swatch.color_list_row import ColorListRow
from swatch.color_grid_box import ColorGridBox


@Gtk.Template(resource_path='/org/gabmus/swatch/ui/palette_view.ui')
class PaletteView(Adw.Bin):
    __gtype_name__ = 'PaletteView'
    stack: Gtk.Stack = Gtk.Template.Child()
    listbox: Gtk.ListBox = Gtk.Template.Child()
    flowbox: Gtk.FlowBox = Gtk.Template.Child()
    list_sw = Gtk.Template.Child()
    grid_sw = Gtk.Template.Child()
    empty_state = Adw.StatusPage = Gtk.Template.Child()
    palette = None

    def __init__(self):
        self.confman = ConfManager()
        super().__init__()
        self.view_list = False
        self.set_view_list(self.confman.conf['palette_view_list'])

    def decide_show_empty_state(self, *_):
        self.stack.set_visible_child(
            self.empty_state if (
                not self.palette or len(self.palette.color_store) <= 0
            ) else (
                self.list_sw if self.view_list else self.grid_sw
            )
        )

    def set_palette(self, palette: Palette):
        self.palette = palette
        self.listbox.bind_model(
            self.palette.color_store, self.__create_list_row, None
        )
        self.flowbox.bind_model(
            self.palette.color_store, self.__create_grid_box, None
        )
        self.palette.color_store.connect(
            'items-changed', self.decide_show_empty_state
        )
        self.decide_show_empty_state()

    def __create_list_row(self, color: Color, *_) -> ColorListRow:
        row = ColorListRow()
        row.set_color(color)
        row.connect('delete-request', self.on_delete_request)
        return row

    def __create_grid_box(self, color: Color, *_) -> ColorGridBox:
        box = ColorGridBox()
        box.set_color(color)
        box.connect('delete-request', self.on_delete_request)
        return box

    def on_delete_request(self, caller: Union[ColorListRow, ColorGridBox]):
        if caller.color is None:
            return
        dialog = Gtk.MessageDialog(
            transient_for=self.get_root(),
            modal=True,
            message_type=Gtk.MessageType.QUESTION,
            buttons=Gtk.ButtonsType.YES_NO,
            text=_('Do you want to delete "{0}" ({1})?').format(
                caller.color.name, caller.color.hex()
            )
        )

        def on_response(_dialog, res):
            _dialog.close()
            if res == Gtk.ResponseType.YES:
                self.palette.color_store.remove_color(caller.color)
                self.get_root().save_palette()

        dialog.connect('response', on_response)
        dialog.present()

    def set_view_list(self, view_list: bool):
        self.view_list = view_list
        self.decide_show_empty_state()
