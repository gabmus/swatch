from typing import List
from gi.repository import Gio, Gtk
from swatch.color import Color


class ColorStore(Gtk.SortListModel):
    def __init__(self):
        self.sorter = Gtk.CustomSorter()
        self.sorter.set_sort_func(self.__sort_func)
        self.list_store = Gio.ListStore(item_type=Color)
        super().__init__(model=self.list_store, sorter=self.sorter)

    def __eq__(self, other):
        if len(self) != len(other):
            return False
        for c1, c2 in zip(self, other):
            if c1 != c2:
                return False
        return True

    def __ne__(self, other):
        return not self == other

    def __sort_func(self, c1: Color, c2: Color, *_) -> int:
        return -1 if c1.name < c2.name else 1

    def invalidate_sort(self):
        self.sorter.set_sort_func(self.__sort_func)

    def add_color(self, n_color: Color):
        for prop in ('name', 'color'):
            n_color.connect(
                f'notify::{prop}', lambda *_: self.invalidate_sort()
            )
        self.list_store.append(n_color)

    def remove_color(self, target: Color):
        for i, color in enumerate(self.list_store):
            if not color:
                continue
            if color == target:
                self.list_store.remove(i)
                return

    def to_list(self) -> List[dict]:
        return [
            {
                'name': color.name,
                'red': color.color[0],
                'green': color.color[1],
                'blue': color.color[2]
            } for color in self.list_store if color
        ]
