from gi.repository import Gtk
from swatch.confManager import ConfManager


@Gtk.Template(resource_path='/org/gabmus/swatch/ui/headerbar.ui')
class GHeaderbar(Gtk.WindowHandle):
    __gtype_name__ = 'GHeaderbar'
    headerbar: Gtk.HeaderBar = Gtk.Template.Child()
    menu_btn: Gtk.MenuButton = Gtk.Template.Child()
    add_btn: Gtk.Button = Gtk.Template.Child()

    def __init__(self):
        super().__init__()
        self.confman = ConfManager()
