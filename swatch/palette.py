from typing import Optional, Tuple
from gi.repository import GObject
from swatch.color import Color
from swatch.color_store import ColorStore
from swatch.confManager import ConfManager


class Palette(GObject.Object):
    def __init__(self, name: str):
        self.__name = name
        self.window_size = (300, 350)
        self.color_store = ColorStore()
        super().__init__()
        self.confman = ConfManager()

    def populate(self):
        palette_d = self.confman.conf['palettes'].get(
            self.__name, {'colors': [], 'window_size': [300, 350]}
        )
        for color_d in palette_d['colors']:
            color = Color(color_d['red'], color_d['green'], color_d['blue'])
            color.name = color_d['name']
            self.color_store.add_color(color)
        self.window_size = tuple(palette_d.get('window_size', [300, 350]))

    def __eq__(self, other):
        return (
            other is not None and
            self.name == other.name and
            self.color_store == other.color_store
        )

    def __ne__(self, other):
        return not self == other

    @GObject.Property(type=str)
    def name(self) -> str:
        return self.__name

    @name.setter
    def name(self, n_name: str):
        if self.__name in self.confman.conf['palettes'].keys():
            d = self.confman.conf['palettes'].pop(self.__name)
            self.confman.conf['palettes'][n_name] = d
        self.__name = n_name

    def save_to_config(self, window_size: Optional[Tuple[int, int]] = None):
        self.confman.conf['palettes'][self.__name] = {
            'colors': self.color_store.to_list()
        }
        if window_size is not None:
            self.confman.conf['palettes'][self.__name]['window_size'] = [
                *window_size
            ]
        self.confman.save_conf()

    def to_gimp_palette(self) -> str:
        out = 'GIMP Palette\n'
        out += '#Palette Name: {0}\n'.format(self.name)
        out += '\n'.join([
            '{0} {1} {2} {3}'.format(
                *c.color, c.name
            ) for c in self.color_store
        ])
        return out
