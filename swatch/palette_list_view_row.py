from gi.repository import Gtk, GObject
from swatch.action_helper import new_action, new_action_group
from swatch.color_bin import ColorBin
from swatch.palette import Palette
from swatch.accel_manager import add_longpress_accel, add_mouse_button_accel


@Gtk.Template(resource_path='/org/gabmus/swatch/ui/palette_list_view_row.ui')
class PaletteListViewRow(Gtk.ListBoxRow):
    __gtype_name__ = 'PaletteListViewRow'
    __gsignals__ = {
        'delete-request': (
            GObject.SignalFlags.RUN_LAST,
            None,
            (GObject.TYPE_PYOBJECT, )
        )
    }
    label: Gtk.Label = Gtk.Template.Child()
    color_bin1: ColorBin = Gtk.Template.Child()
    color_bin2: ColorBin = Gtk.Template.Child()
    color_bin3: ColorBin = Gtk.Template.Child()
    color_bin4: ColorBin = Gtk.Template.Child()
    popover: Gtk.PopoverMenu = Gtk.Template.Child()
    palette = None

    def __init__(self):
        super().__init__()
        self.color_bins = (
            self.color_bin1, self.color_bin2, self.color_bin3, self.color_bin4
        )

        # longpress & right click
        self.longpress = add_longpress_accel(
            self, lambda *args: self.popover.popup()
        )
        self.rightclick = add_mouse_button_accel(
            self,
            lambda gesture, *args:
                self.popover.popup()
                if gesture.get_current_button() == 3  # 3 is right click
                else None
        )
        new_action_group(self, 'palette', [
            new_action('delete', self.on_delete)
        ])

    def on_delete(self, *_):
        self.emit('delete-request', self.palette)

    def set_palette(self, palette: Palette):
        self.palette = palette
        self.label.set_text(palette.name)
        for cb in self.color_bins:
            cb.set_visible(False)
        for color, cb in zip(self.palette.color_store, self.color_bins):
            cb.set_color(color)
            cb.set_visible(True)
